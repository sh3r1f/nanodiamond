using UnityEngine;

public class MoveBlock : MonoBehaviour {

  private float time  = 0;

  void FixedUpdate()
  {
    var horizontal = Input.GetAxis("Horizontal");
    gameObject.transform.Rotate(new Vector3(0,horizontal,0));

    time += Time.deltaTime%10;
    
    if(!Input.anyKeyDown && time > 5f)
    {
      gameObject.transform.Rotate(Vector3.up*0.5f);
    }

    if(Input.anyKeyDown){
      time = 0;
    }
  }

  public void LeftBtn()
  {
    gameObject.transform.Rotate(Vector3.down * 10f);
  }

  public void RightBtn()
  {
    gameObject.transform.Rotate(Vector3.up * 10f);
  }

}